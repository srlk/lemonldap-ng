###
LemonLDAP::NG Favorite Applications script
###

# FavApps function (launched by "star" icon)
FavApps = (star) ->
	$.ajax
		type: "POST"
		url: "#{portal}favapps"
		data:
			id: star.attr 'aid'
			uri: star.attr 'uri'
			name: star.attr 'name'
			logo: star.attr 'logo'
			desc: star.attr 'desc'
		dataType: 'json'
		success: (resp) ->
			if resp.error
				console.log 'Max number reached'
				switchStar star, 0
			else if resp.error == 0
					console.log 'Not authorized'
					switchStar star, 0
				else if resp.result
						console.log 'App. registered'
						switchStar star, 1
					else
						console.log 'App. unregistered'
						switchStar star, 0
		error: switchStar star, '0'

ResetFavApps = ->
	$.ajax
		type: "DELETE"
		url: "#{portal}favapps"
		success: (resp) ->
			if resp.result
				console.log 'Favorite Apps. reset'
				window.location.reload()
			else
				console.log 'Error'

switchStar = (star, status) ->
	star.attr('src', "#{window.staticPrefix}common/icons/star#{status}.png")
	#window.location.reload()

# Switch "star" ans Reset events
$(document).ready ->
	$('body').on 'click', '.star', () -> FavApps $(this)
	$('#reset').on 'click', () -> ResetFavApps()
