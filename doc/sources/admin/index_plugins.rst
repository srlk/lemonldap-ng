Plugins
=======

.. toctree::
   :maxdepth: 1

   autosignin
   bruteforceprotection
   cda
   checkstate
   checkuser
   viewer
   contextswitching
   plugincustom
   decryptvalue
   loginhistory
   forcereauthn
   globallogout
   grantsession
   impersonation
   notifications
   status
   public_pages
   refreshsessionapi
   resetpassword
   resetcertificate
   restservices
   soapservices
